<?php

declare(strict_types=1);

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\Symfony\Phpunit;

use Closure;

class MockDto
{
    public function __construct(
        private readonly string $class,
        private readonly ?array $construct = null,
        private readonly bool $partial = false,
        private readonly ?Closure $onCreate = null,
    ) {}

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function getConstruct(): ?array
    {
        return $this->construct;
    }

    public function getPartial(): ?bool
    {
        return $this->partial;
    }

    public function getOnCreate(): ?Closure
    {
        return $this->onCreate;
    }
}
